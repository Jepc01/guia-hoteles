$(function(){
  //toltip
  $("[data-toggle='tooltip']").tooltip();
  // popovers
  $("[data-toggle='popover']").popover();
  // Control de rotacion del carousel
  $('.carousel').carousel({
    interval:1500
  });
  //Eventos
  $('#contacto').on('show.bs.modal',function(e){
    console.log('el modal contacto se esta mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled',true); //prop para acceder a las propiedades de los elementos seleccionados
  });

  $('#contacto').on('shown.bs.modal',function(e){
    console.log('el modal contacto se mostro');
  });
  $('#contacto').on('hide.bs.modal',function(e){
    console.log('el modal contacto se oculta');
  });
  $('#contacto').on('hidden.bs.modal',function(e){
    console.log('el modal contacto se oculto');
    $('#contactoBtn').prop('disabled',false);
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
  });
});